import React, { useState } from "react";
import WeatherDetail from "./WeatherDetail";
import "./weather.css";

function Weather() {
    const [weather, setWeather] = useState([]);
    const [weatherDetail, setWeatherDetail] = useState([]);
    const [form, setForm] = useState({
        city: "",
    });

    async function weatherData(e) {
        e.preventDefault();
        if (form.city == "") {
            alert("Provide City Value");
        } else {
            const data = await fetch(
                `https://www.metaweather.com/api/location/search/?query=${form.city}`
            )
                .then((res) => res.json())
                .then((data) => data);
            if (data.length === 0) {
                alert('No Record Found');
            } else {
                locationDetails(data[0].woeid);
                setWeather({ data: data[0].woeid });
            }
        }
    }

    const locationDetails = (woeid) => {
        const url = 'https://www.metaweather.com/api/location/' + woeid;
        fetch(url).then((resp) => resp.json())
            .then(function (data) {
                setWeatherDetail({ data: data });
            })
            .catch(function (error) {
            });
    }

    const handleChange = (e) => {
        let name = e.target.name;
        let value = e.target.value;

        if (name == "city") {
            setForm({ ...form, city: value });
        }
    };
    return (
        <div className="weather">
            <span className="title">Volvo Tomorrow's Weather</span>
            <br />
            <form>
                <input
                    type="text"
                    placeholder="city"
                    name="city"
                    onChange={(e) => handleChange(e)}
                />
                <button className="getweather" onClick={(e) => weatherData(e)}>Submit</button>
            </form>

            <div>
                <WeatherDetail data={weatherDetail.data} />
            </div>
        </div>
    );
}

export default Weather;