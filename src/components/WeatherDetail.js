import React from "react";
import "./weatherDetail.css";

function WeatherDetail(props) {
    const { data } = props;
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);

    const iconurl =
        "https://www.metaweather.com/static/img/weather/png/64/" +
        `${data !== undefined ? data.consolidated_weather[1].weather_state_abbr : null}` +
        ".png";
    return (
        <div className="weatherDetail">
            {data !== undefined ?

                <React.Fragment>
                    <div className="maincard">
                        <span className="cardtitle">{data.title}'s Weather</span>

                        <h1>
                            {" "}
                            {Math.floor(data.consolidated_weather[1].the_temp)}
                            <sup>o</sup>
                        </h1>
                        
                        <img className="weather-icon" src={iconurl} />
                        <span className="weather-description">
                            {" "}
                            {data.consolidated_weather[1].weather_state_name}
                        </span>
                    </div>
                    <div className="weatherdetails">
                        <div className="section1">
                            <table>
                                <tr>
                                    <td>
                                        <h4>High/Low</h4>
                                    </td>
                                    <td>
                                        <span>
                                            {Math.floor(data.consolidated_weather[1].max_temp)}/
                                            {Math.floor(data.consolidated_weather[1].min_temp)}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Humidity</h4>
                                    </td>
                                    <td>
                                        <span>{data.consolidated_weather[1].humidity}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Predictability</h4>
                                    </td>
                                    <td>
                                        <span>{data.consolidated_weather[1].predictability}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Dir Com</h4>
                                    </td>
                                    <td>
                                        <span>{data.consolidated_weather[1].wind_direction_compass}</span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div className="section2">
                            <table>
                                <tr>
                                    <td>
                                        <h4>Air Pressure</h4>
                                    </td>
                                    <td>
                                        <span>{data.consolidated_weather[1].air_pressure}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Visibility</h4>
                                    </td>
                                    <td>
                                        <span>
                                            {(data.consolidated_weather[1].visibility).toFixed(2)}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Wind Dir</h4>
                                    </td>
                                    <td>
                                        <span>
                                            {(data.consolidated_weather[1].wind_direction).toFixed(2)}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Wind Spd</h4>
                                    </td>
                                    <td>
                                        <span>
                                            {(data.consolidated_weather[1].wind_speed).toFixed(2)}
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </React.Fragment> : null
            }
        </div>
    );
}

export default WeatherDetail;