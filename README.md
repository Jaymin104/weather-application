# REACT WEATHER APP #

This README would normally document whatever steps are necessary to get your application up and running.

### Table of Contents ###

* Instruction
* Description
* Technologies

### Instruction ###

* Clone the Project

* $ git clone git clone https://Jaymin104@bitbucket.org/Jaymin104/weather-application.git

* Install dependencies. Make sure you already have nodejs & npm installed in your system.
* $ npm install

* Run it
* $ npm start
* Runs the app in the development mode.
* Open http://localhost:3000 to view it in the browser.

* The page will reload if you make edits.
* You will also see any lint errors in the console.

### Description ###

* Simple React Application for Assessment.

### Technologies ###

* React and Typescript